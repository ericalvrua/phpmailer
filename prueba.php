<?php

require 'src/Exception.php';
require 'src/PHPMailer.php';
require 'src/SMTP.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

$mail = new PHPMailer(true);                              
try {
    //Server settings
    $mail->SMTPDebug = 0;                                 
    $mail->isSMTP();                                      
    $mail->Host = 'smtp.gmail.com';  
    $mail->SMTPAuth = true;                               
    $mail->Username = 'ericalvaradoruano@gmail.com';                
    $mail->Password = '';                           
    $mail->SMTPSecure = 'tls';                            
    $mail->Port = 587;                                    

    //Recipients
    $mail->setFrom('ericalvaradoruano@gmail.com', 'Eric Alvarado');
    $mail->addAddress('ericjackson199@gmail.com');     
    $mail->addAddress('alberto.sarabia@edosoft.es'); 
    $mail->addAddress('david.alvarez@edosoft.es');              

    //Content
    $mail->isHTML(true);                                  
    $mail->Subject = 'Se envia correo de prueba de PHP Mailer';
    $mail->Body    = 'Esto es una prueba';
    $mail->AltBody = 'Esto es una prueba';

    $mail->send();
    echo 'Mensaje enviado';
} catch (Exception $e) {
    echo 'Error: No se ha podido enviar';
    echo 'Mailer Error: ' . $mail->ErrorInfo;
}